from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


# Create your views here.
def home(request):
    return render(request, "members/home.html")

def signup(request): 
    if request.method == "POST":
        username = request.POST["username"]
        fname = request.POST["fname"]
        lname = request.POST["lname"]
        email = request.POST["email"]
        password = request.POST["password"]
        pass2 = request.POST["pass2"]

        if User.objects.filter(username=username):
            messages.info(request, "Username is already exist")
            return redirect(signup)

        elif len(username) > 10:
            messages.info(request, "Username too long")
            return redirect(signup)

        elif password != pass2:
            messages.error(request, "Password didn't match")
            return redirect(signup)

        else:
            user = User.objects.create_user(
                username=username,
                password=password,
                email=email,
                first_name=fname,
                last_name=lname,
            )

            user.set_password(password)

            user.is_staff = True

            user.save()
            return redirect("signin")

    return render(request, "members/signup.html")


def signin(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            return render(request, "members/profil.html")
        else:
            messages.info(request, "Invalid Username or password")
            return redirect("signin")

    return render(request, "members/signin.html")


def logoutUser(request):
    logout(request)
    return redirect("home")
